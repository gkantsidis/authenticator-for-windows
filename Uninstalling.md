# Uninstalling Authenticator

If the application is already installed, then deployment may fail.
In that case, try the following from PowerShell (admin):

```PowerShell
get-appxpackage -all |? { $_.Name -eq "56672RoelvandeWater.AuthenticatorforWindows" } | Remove-AppxPackage
```

In addition remove the directory under `%LOCALAPPDATA%\Packages`
(probably the directory is also named `56672RoelvandeWater.AuthenticatorforWindows`).

See [here](https://blogs.msdn.microsoft.com/wsdevsol/2013/01/28/registration-of-the-app-failed-another-user-has-already-installed-a-packaged-version-of-this-app-an-unpackaged-version-cannot-replace-this/)
and [here](https://www.mrlacey.com/2017/01/launch-store-installed-app-from-command.html).